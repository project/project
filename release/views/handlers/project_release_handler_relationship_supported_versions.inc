<?php

/**
 * Views handler for relationships between releases and supported version info.
 */
class project_release_handler_relationship_supported_versions extends views_handler_relationship {

  /**
   * {@inheritdoc}
   */
  public function init(&$view, &$options) {
    parent::init($view, $options);
    // This relationship only works with {project_release_supported_versions}.
    $this->definition['base'] = 'project_release_supported_versions';
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensure_my_table();
    $version_alias = $this->query->ensure_table('field_data_field_release_version');
    $join = new views_join();
    $alias = $this->definition['base'] . '_' . $this->table_alias;
    $join->definition = [
      'table' => $this->definition['base'],
      'field' => 'nid',
      'left_table' => $this->table_alias,
      'left_field' => $this->field,
      'extra' => $version_alias . '.field_release_version_value LIKE concat(' . $alias . ".branch, '%')"
      . ' AND ' . $version_alias . '.field_release_version_value NOT LIKE concat(' . $alias . ".branch, '%.%')"
      . ' AND ' . $alias . '.supported = 1',
    ];
    if (!empty($this->options['required'])) {
      $join->definition['type'] = 'INNER';
    }
    $join->construct();
    $this->alias = $this->query->add_relationship($alias, $join, $this->definition['base'], $this->relationship);
  }

}
