<?php

/**
 * @file
 * List of all releases associated with a particular project.
 */

$view = new view();
$view->name = 'project_release_by_project';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Project releases by project';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Releases for [project]';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Filter';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'node';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Project (field_release_project) */
$handler->display->display_options['arguments']['field_release_project_target_id']['id'] = 'field_release_project_target_id';
$handler->display->display_options['arguments']['field_release_project_target_id']['table'] = 'field_data_field_release_project';
$handler->display->display_options['arguments']['field_release_project_target_id']['field'] = 'field_release_project_target_id';
$handler->display->display_options['arguments']['field_release_project_target_id']['default_action'] = 'not found';
$handler->display->display_options['arguments']['field_release_project_target_id']['title_enable'] = TRUE;
$handler->display->display_options['arguments']['field_release_project_target_id']['title'] = 'Releases for %1';
$handler->display->display_options['arguments']['field_release_project_target_id']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['field_release_project_target_id']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_release_project_target_id']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_release_project_target_id']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_release_project_target_id']['specify_validation'] = TRUE;
$handler->display->display_options['arguments']['field_release_project_target_id']['validate']['type'] = 'node';
/* Filter criterion: Content: Published or admin */
$handler->display->display_options['filters']['status_extra']['id'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['table'] = 'node';
$handler->display->display_options['filters']['status_extra']['field'] = 'status_extra';
$handler->display->display_options['filters']['status_extra']['group'] = 1;
/* Filter criterion: Project: Project system behavior */
$handler->display->display_options['filters']['project_type']['id'] = 'project_type';
$handler->display->display_options['filters']['project_type']['table'] = 'node';
$handler->display->display_options['filters']['project_type']['field'] = 'project_type';
$handler->display->display_options['filters']['project_type']['value'] = 'project_release';
$handler->display->display_options['filters']['project_type']['group'] = 1;
/* Filter criterion: Content: Version (field_release_version) */
$handler->display->display_options['filters']['field_release_version_value']['id'] = 'field_release_version_value';
$handler->display->display_options['filters']['field_release_version_value']['table'] = 'field_data_field_release_version';
$handler->display->display_options['filters']['field_release_version_value']['field'] = 'field_release_version_value';
$handler->display->display_options['filters']['field_release_version_value']['operator'] = 'starts';
$handler->display->display_options['filters']['field_release_version_value']['exposed'] = TRUE;
$handler->display->display_options['filters']['field_release_version_value']['expose']['operator_id'] = 'field_release_version_value_op';
$handler->display->display_options['filters']['field_release_version_value']['expose']['label'] = 'Version starts with';
$handler->display->display_options['filters']['field_release_version_value']['expose']['operator'] = 'field_release_version_value_op';
$handler->display->display_options['filters']['field_release_version_value']['expose']['identifier'] = 'version';
$handler->display->display_options['filters']['field_release_version_value']['expose']['remember_roles'] = array(
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'node/%/release';

/* Display: Feed */
$handler = $view->new_display('feed', 'Feed', 'feed');
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['style_plugin'] = 'rss';
$handler->display->display_options['row_plugin'] = 'node_rss';
$handler->display->display_options['row_options']['item_length'] = 'rss';
$handler->display->display_options['path'] = 'node/%/release/feed';
$handler->display->display_options['displays'] = array(
  'default' => 'default',
  'page' => 'page',
);
$translatables['project_release_by_project'] = array(
  t('Master'),
  t('Releases for [project]'),
  t('more'),
  t('Filter'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('All'),
  t('Releases for %1'),
  t('Version starts with'),
  t('Page'),
  t('Feed'),
);
