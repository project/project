<?php

/**
 * @file
 * Code for the "releases" subtab to the edit tab on project nodes.
 */

/**
 * Releases subtab for the project edit tab.
 */
function project_release_project_edit_releases($node) {
  project_project_set_breadcrumb($node);
  drupal_set_title(check_plain($node->title));
  return drupal_get_form('project_release_project_edit_form', $node);
}

/**
 * Form callback for edit relases page.
 *
 * @see project_release_project_edit_releases()
 */
function project_release_project_edit_form($form, $form_state, $node) {
  // Get all the data about versions for this project.
  if ($api_vid = variable_get('project_release_api_vocabulary')) {
    $api_field = 'taxonomy_' . taxonomy_vocabulary_load($api_vid)->machine_name;
    $recommended_api_terms = (new EntityFieldQuery())->entityCondition('entity_type', 'taxonomy_term')
      ->fieldCondition('field_release_recommended', 'value', 1)
      ->execute();
  }
  $query = db_select('project_release_supported_versions', 'psrv')
    ->condition('psrv.nid', $node->nid)
    ->fields('psrv', ['branch', 'supported', 'recommended']);
  $data = [];
  $current_versions = [];
  foreach ($query->execute() as $object) {
    // Releases with an API compatability that is not recommended can not be
    // supported.
    if ($versions = project_release_get_releases_by_version($node->nid, $object->branch)) {
      $current_versions[$object->branch] = entity_metadata_wrapper('node', array_shift($versions));
      if (isset($api_field) && ($api_tid = project_release_get_release_api_tid($current_versions[$object->branch]->raw())) && !isset($recommended_api_terms['taxonomy_term'][$api_tid])) {
        continue;
      }
    }

    $data[$object->branch] = [
      'name' => $object->branch . '*',
      'supported' => (bool) $object->supported,
      'recommended' => (bool) $object->recommended,
    ];
  }
  if (empty($data)) {
    $form['help'] = [
      '#markup' => '<p>' . t('No release versions for this project exist.') . '</p>',
    ];
  }
  else {
    $form['help'] = [
      '#markup' => '<p>' . t('For each branch of this project, this page allows you to define which are supported and recommended by you and other maintainers of this project.') . '</p>',
    ];
    // Build the form elements for supported and recommended major versions.
    $form['branches'] = [
      '#tree' => TRUE,
      '#theme' => 'project_release_edit_table',
    ];
    uksort($data, 'project_release_version_compare');
    foreach (array_reverse($data, TRUE) as $branch => $branch_data) {
      $form['branches'][$branch] = [
        'name' => [
          '#markup' => check_plain($branch_data['name']),
        ],
        'current' => [
          '#markup' => (!empty($current_versions[$branch])) ? $current_versions[$branch]->field_release_version->value(['sanitize' => TRUE]) : '',
        ],
        'supported' => [
          '#type' => 'checkbox',
          '#title' => t('Supported'),
          '#title_display' => 'invisible',
          '#default_value' => $branch_data['supported'],
        ],
        'recommended' => [
          '#type' => 'checkbox',
          '#title' => t('Recommended'),
          '#title_display' => 'invisible',
          '#default_value' => $branch_data['recommended'],
          '#states' => [
            // Disable so value is not submitted if checked while becoming
            // invisible. Make invisible because disabled checkboxes without
            // labels don’t have great contrast versus unchecked boxes.
            'disabled' => [
              ':input[name="branches[' . $branch . '][supported]"]' => ['checked' => FALSE],
            ],
            'invisible' => [
              ':input[name="branches[' . $branch . '][supported]"]' => ['checked' => FALSE],
            ],
          ],
        ],
      ];

      // Check if the maintainer is allowed to mark the branch as supported.
      if (($supportable_branch_pattern = variable_get('project_release_supportable_branch_pattern_' . $node->type, variable_get('project_release_supportable_branch_pattern'))) && !preg_match($supportable_branch_pattern, $branch)) {
        $form['branches'][$branch]['supported']['#disabled'] = TRUE;
        $form['branches'][$branch]['supported']['#value'] = 0;
        $form['branches'][$branch]['recommended']['#disabled'] = TRUE;
        $form['branches'][$branch]['recommended']['#value'] = 0;
      }

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save'),
      ];
    }
  }

  $types = project_release_release_node_types();
  // @todo this uses the first release node type. What to do if there are
  // multiple?
  $links['links'] = [
    'add_new_release' => [
      'title' => t('Add new release'),
      'href' => 'node/add/' . str_replace('_', '-', reset($types)) . '/' . $node->nid,
    ],
  ];

  $form['create_link'] = [
    '#markup' => '<p>' . theme('links', $links) . '</p>',
  ];

  return $form;
}

/**
 * Theme callback, render elements into a table.
 *
 * @see project_release_project_edit_form()
 */
function theme_project_release_edit_table($v) {
  foreach (element_children($v['branches']) as $branch) {
    $rows[] = [
      [
        'header' => TRUE,
        'data' => drupal_render($v['branches'][$branch]['name']),
      ],
      drupal_render($v['branches'][$branch]['current']),
      drupal_render($v['branches'][$branch]['supported']),
      drupal_render($v['branches'][$branch]['recommended']),
    ];
  }

  return theme('table', [
    'header' => [
      t('Series'),
      t('Current release'),
      t('Supported'),
      t('Recommended'),
    ],
    'rows' => $rows,
  ]);
}

/**
 * Validates the project form regarding release-specific fields.
 *
 * @see project_release_project_edit_releases()
 */
function project_release_project_edit_form_validate($form, &$form_state) {
  foreach ($form_state['values']['branches'] as $branch => $values) {
    if ($values['recommended'] && !$values['supported']) {
      form_error($form['branches'][$branch]['recommended'], t('You can not recommend %branch that is not supported.', [
        '%branch' => $form['branches'][$branch]['name']['#markup'],
      ]));
    }
  }
}

/**
 * Submit handler when project admins use the releases subtab.
 *
 * @see project_release_project_edit_releases()
 */
function project_release_project_edit_form_submit($form, &$form_state) {
  $nid = $form_state['build_info']['args'][0]->nid;

  $updates = [];
  foreach ($form_state['values']['branches'] as $branch => $values) {
    $updated = db_update('project_release_supported_versions')
      ->condition('nid', $nid)
      ->condition('branch', $branch)
      ->fields(array_map('intval', $values))
      ->execute();
    if ($updated) {
      $updates[] = $branch;
    }
  }

  drupal_set_message(t('Release settings have been saved.'));

  // Since we've possibly adjusted the status/recommended releases, we need to
  // recompute the update status for each set of releases.
  foreach ($updates as $branch) {
    project_release_compute_update_status($nid, $branch, FALSE);
  }
  // Regenerate the download table after the last update, so it isn’t
  // needlessly updated.
  project_release_download_table($nid, TRUE);
}
