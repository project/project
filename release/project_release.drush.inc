<?php

/**
 * @file
 * Drush command for packaging files for release nodes.
 *
 * @author Derek Wright (http://drupal.org/user/46549)
 */

/**
 * Implements hook_drush_command().
 */
function project_release_drush_command() {
  return array(
    'release-package' => array(
      'description' => 'Package files for release nodes.',
      'arguments' => array(
        'type' => '"branch" or "tag", required.',
        'project_id' => 'Project, either a Node ID or machine name. Required.',
        'release_nid' => 'Release nid',
      ),
    ),
    'release-create-history' => array(
      'description' => 'Creates XML release history for projects.',
      'arguments' => array(
        'project_id' => 'Project, either a Node ID or machine name. Omit to build all projects.',
      ),
    ),
  );
}

/**
 * Implements hook_drush_help().
 */
function project_release_drush_help($section) {
  switch ($section) {
    case 'meta:project_release:title':
      return dt('Project release commands');
  }
}

/**
 * Drush arguments validator for the release-package command.
 */
function drush_project_release_release_package_validate($type = NULL, $project_id = NULL) {
  if ($type !== 'tag' && $type !== 'branch') {
    return drush_set_error('INVALID_TYPE', dt('First type must be "branch" or "tag".'));
  }
}

/**
 * Drush callback for the release-package command.
 */
function drush_project_release_release_package($type, $project_id, $release_nid) {
  // Force the right umask while this script runs, so that everything is created
  // with sane file permissions.
  umask(0022);

  // Load the include file for packager-related helper functions.
  module_load_include('inc', 'project_release', 'includes/packager');

  // If needed, prod module_implements() to recognize our
  // project_release_watchdog() implementation.
  if (!in_array('project_release', module_implements('watchdog'))) {
    module_implements('watchdog', FALSE, TRUE);
  }

  // Find the project.
  $project_node = project_load($project_id);
  if ($project_node === FALSE) {
    return drush_set_error('INVALID_PROJECT', dt('ERROR: Project ID @id not found', array('@id' => $project_id)));
  }
  if ($project_node->status != NODE_PUBLISHED) {
    return drush_set_error('UNPUBLISHED_PROJECT', dt('ERROR: Project @id not published', array('@id' => $project_id)));
  }
  if (!$project_node->field_project_has_releases[LANGUAGE_NONE][0]['value']) {
    return drush_set_error('NO_RELEASES_PROJECT', dt('ERROR: Project @id does not have releases', array('@id' => $project_id)));
  }

  if (!project_release_package_release($release_nid, $project_node)) {
    return drush_set_error('Packaging Failure', dt('ERROR: Project @id failed to package', array('@id' => $project_id)));
  }
}

/**
 * Package a single release.
 *
 * @param int $release_nid
 *   A release node ID.
 * @param object $project_node
 *   A project node object.
 */
function project_release_package_release($release_nid, $project_node) {
  // We don't want to waste too much RAM by leaving all these loaded nodes
  // in RAM, so we reset the node_load() cache each time we call it.
  $return = TRUE;
  $release_node = node_load($release_nid, NULL, TRUE);
  $release_wrapper = entity_metadata_wrapper('node', $release_node);
  if (empty($release_node)) {
    watchdog('package_error', "Can't load release node for release ID %nid", ['%nid' => $release_node->nid], WATCHDOG_ERROR);
    return FALSE;
  }

  // If file is not empty, a tag release is already packaged.
  if ($release_wrapper->field_release_build_type->value() === 'static' && !empty($release_node->field_release_files)) {
    return TRUE;
  }
  // If published and file is not empty, a branch release is already
  // packaged. Published releases may be repackaged.
  if ($release_wrapper->field_release_build_type->value() === 'dynamic' && $release_node->status != NODE_PUBLISHED && !empty($release_node->field_release_files)) {
    return TRUE;
  }

  // Find the packager plugin.
  $packager = project_release_get_packager_plugin($release_node, drush_tempdir());
  if ($packager === FALSE) {
    watchdog('package_error', "Can't find packager plugin to use for %release", ['%release' => $release_node->title], WATCHDOG_ERROR);
    return FALSE;
  }

  drush_log(dt('Packaging @title (nid @nid)', [
    '@title' => $release_node->title,
    '@nid' => $release_node->nid,
  ]));

  // Delete existing log messages.
  db_delete('project_release_package_errors')
    ->condition('nid', $release_node->nid)
    ->execute();
  project_release_watchdog(NULL, $release_node->nid);

  $files = [];
  $rval = $packager->createPackage($files);

  switch ($rval) {
    case 'error':
      $return = FALSE;
      break;

    case 'success':
    case 'rebuild':
      project_release_packager_update_node($release_node, $files);
      project_release_check_supported_versions($release_node);
      if ($rval === 'rebuild') {
        $msg = '%release_title has changed, re-packaged.';
      }
      else {
        $msg = 'Packaged %release_title.';
      }
      watchdog('package_' . $release_wrapper->field_release_build_type->value(), $msg, ['%release_title' => $release_node->title], WATCHDOG_INFO, l(t('View'), 'node/' . $release_node->nid));
      break;
  }

  // Write any log entries.
  project_release_watchdog(NULL, FALSE);
  return $return;
}

/**
 * Drush-only implementation of hook_watchdog().
 *
 * Writes out messages to project_release_package_errors table.
 *
 * @param array $log_entry
 *   An associative array, see hook_watchdog() for keys.
 * @param int|bool $new_nid
 *   Set release nid for log entries to be stored with. FALSE to reset and
 *   write out any entries.
 */
function project_release_watchdog($log_entry = NULL, $new_nid = NULL) {
  static $nid = FALSE;
  static $messages = array();

  if (is_null($new_nid)) {
    if (!is_array($log_entry['variables'])) {
      $log_entry['variables'] = array();
    }
    // Invoked via hook_watchdog(), store message if there is a current node.
    if ($nid !== FALSE && $log_entry['severity'] < WATCHDOG_NOTICE) {
      $messages[] = t($log_entry['message'], $log_entry['variables']);
    }
  }
  else {
    // Clearing out nid, write out messages.
    if ($new_nid === FALSE && !empty($messages)) {
      db_insert('project_release_package_errors')->fields(array(
        'nid' => $nid,
        'messages' => serialize($messages),
      ))->execute();
      $messages = array();
    }

    // Store new nid.
    $nid = $new_nid;
  }
}

/**
 * Drush callback for the release-create-history command.
 */
function drush_project_release_release_create_history($project_id = NULL) {
  // Force the right umask while this script runs, so that everything is created
  // with sane file permissions.
  umask(0022);

  if (is_null($project_id)) {
    drush_log(dt('Generating XML release history files for all projects.'), 'ok');
  }
  else {
    $project_node = project_load($project_id);
    if ($project_node === FALSE) {
      return drush_set_error('INVALID_PROJECT', dt('Project ID @id not found.', array('@id' => $project_id)));
    }
  }

  if (isset($project_node)) {
    project_release_history_generate_project_xml($project_node);
  }
  else {
    // Generate all xml files for projects with releases.
    $result = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', project_project_node_types())
      ->fieldCondition('field_project_has_releases', 'value', TRUE)
      ->execute();
    if (!empty($result)) {
      foreach (array_chunk(array_keys($result['node']), variable_get('project_release_history_batch_size', 100)) as $nids) {
        foreach (node_load_multiple($nids, [], TRUE) as $node) {
          project_release_history_generate_project_xml($node);
        }

        // Reset caches so we aren't leaking memory.
        entity_get_controller('field_collection_item')->resetCache();
        drupal_static_reset('field_language');
        drupal_lookup_path('wipe');
        if (function_exists('versioncontrol_get_backends')) {
          foreach (versioncontrol_get_backends() as $backend) {
            $backend->getController('repo')->resetCache();
          }
        }
      }
    }
  }

  // If we're operating on all projects, generate the huge list, too.
  if (is_null($project_id)) {
    $result = db_query('SELECT n.title, n.nid, n.status, n.type, mn.field_project_machine_name_value AS machine_name, u.name AS username FROM {node} n INNER JOIN {field_data_field_project_machine_name} mn ON n.nid = mn.entity_id INNER JOIN {users} u ON n.uid = u.uid');
    $xml = '';
    foreach ($result as $project) {
      $xml .= " <project>\n";
      $xml .= '  <title>' . check_plain($project->title) . "</title>\n";
      $xml .= '  <short_name>' . check_plain($project->machine_name) . "</short_name>\n";
      $xml .= '  <link>' . url('node/' . $project->nid, ['absolute' => TRUE]) . "</link>\n";
      $xml .= '  <dc:creator>' . check_plain($project->username) . "</dc:creator>\n";
      $xml .= '  <type>' . check_plain($project->type) . "</type>\n";
      $term_query = db_query('SELECT v.name AS vocab_name, v.vid, td.name AS term_name, td.tid FROM {taxonomy_index} ti INNER JOIN {taxonomy_term_data} td ON ti.tid = td.tid INNER JOIN {taxonomy_vocabulary} v ON td.vid = v.vid WHERE ti.nid = :nid', [':nid' => $project->nid]);
      $xml_terms = '';
      foreach ($term_query as $term) {
        $xml_terms .= '   <term><name>' . check_plain($term->vocab_name) . '</name>';
        $xml_terms .= '<value>' . check_plain($term->term_name) . "</value></term>\n";
      }
      $xml_terms .= _project_release_compatibility_term($project);

      if (!empty($xml_terms)) {
        $xml .= "  <terms>\n" . $xml_terms . "  </terms>\n";
      }
      if (!$project->status) {
        // If it's not published, we can skip the rest for this project.
        $xml .= "  <project_status>unpublished</project_status>\n";
      }
      else {
        $xml .= "  <project_status>published</project_status>\n";
        // Include a list of API terms if available.
        if ($api_vid = variable_get('project_release_api_vocabulary', 0)) {
          $term_query = db_query('SELECT DISTINCT td.name FROM {field_data_field_release_project} rp INNER JOIN {taxonomy_index} ti ON ti.nid = rp.entity_id INNER JOIN {taxonomy_term_data} td ON ti.tid = td.tid AND td.vid = :api_vid WHERE rp.field_release_project_target_id = :nid ORDER BY td.weight ASC', [
            ':api_vid' => $api_vid,
            ':nid' => $project->nid,
          ]);
          $xml_api_terms = '';
          foreach ($term_query as $api_term) {
            $xml_api_terms .= '   <api_version>' . check_plain($api_term->name) . "</api_version>\n";
          }
          if (!empty($xml_api_terms)) {
            $xml .= "  <api_versions>\n" . $xml_api_terms . "  </api_versions>\n";
          }
        }
      }

      $xml .= " </project>\n";
    }
    if (empty($xml)) {
      return drush_set_error('NO_PROJECTS', dt('No projects found on this server.'));
    }
    return project_release_history_write_xml($xml);
  }
}

/**
 * Generate the XML history file for a project.
 *
 * If a history file already exists for this combination, this function will
 * generate a new history and atomically replace the old one (currently, just
 * logs to watchdog for debugging).
 *
 * @param object $project
 *   Project node entity.
 */
function project_release_history_generate_project_xml(stdClass $project) {
  $vocabularies = taxonomy_vocabulary_get_names();
  $api_vocabulary = variable_get('project_release_api_vocabulary');
  $extra_terms = array(
    'project_core' => 'Drupal core',
    'project_distribution' => 'Distributions',
    'project_module' => 'Modules',
    'project_theme' => 'Themes',
    'project_theme_engine' => 'Theme engines',
    'project_translation' => 'Translations',
  );

  drush_log(dt('Generating release history for @machine_name.', array('@machine_name' => $project->field_project_machine_name[LANGUAGE_NONE][0]['value'])), 'ok');

  $releases_xml = [];

  if ($project->status) {
    foreach (project_release_get_releases_by_version($project->nid) as $release) {
      $release_wrapper = entity_metadata_wrapper('node', $release);
      $xml = new SimpleXMLElement('<root></root>');
      $xml->release->name = $release->title;
      $xml->release->version = $release_wrapper->field_release_version->value(['sanitize' => FALSE]);
      if ($value = $release_wrapper->field_release_vcs_label->value(['sanitize' => FALSE])) {
        $xml->release->tag = $value;
      }
      foreach ([
        'version_major',
        'version_minor',
        'version_patch',
        'version_extra',
      ] as $vers_type) {
        // 0 is a totally legitimate value for any of these version fields, so
        // we need to test with isset() instead of !empty(). However, we do
        // *not* want to include anything in the release history XML if the
        // value is an empty string.
        $value = $release_wrapper->{'field_release_' . $vers_type}->value(['sanitize' => FALSE]);
        if (isset($value) && $value !== '') {
          $xml->release->$vers_type = $value;
        }
      }

      // Need to fetch list of files for this release, so field_collection
      // properly loads host entities.
      if (!empty($release->field_release_files[LANGUAGE_NONE])) {
        $files = array_map('field_collection_field_get_entity', $release->field_release_files[LANGUAGE_NONE]);
      }
      else {
        $files = [];
      }

      $xml->release->status = 'published';
      $xml->release->release_link = url('node/' . $release->nid, ['absolute' => TRUE]);
      if (!empty($release_wrapper->field_release_files[0])) {
        $xml->release->download_link = field_view_value('field_collection_item', $files[0], 'field_release_file', $files[0]->field_release_file[LANGUAGE_NONE][0])['#file']->uri;
        $xml->release->date = $release_wrapper->field_release_files[0]->field_release_file->value()['timestamp'];
        $xml->release->mdhash = $release_wrapper->field_release_files[0]->field_release_file_hash->value(['sanitize' => FALSE]);
        $xml->release->filesize = $release_wrapper->field_release_files[0]->field_release_file->value()['filesize'];
      }

      foreach ($release_wrapper->field_release_files as $key => $file) {
        $xml->release->files->file[$key]->url = field_view_value('field_collection_item', $files[$key], 'field_release_file', $files[$key]->field_release_file[LANGUAGE_NONE][0])['#file']->uri;
        $file_parts = explode('.', $file->field_release_file->value()['filename']);
        $archive_type = array_pop($file_parts);
        // See if the previous extension is '.tar' and if so, add that, so we
        // see 'tar.gz' or 'tar.bz2' instead of just 'gz' or 'bz2'.
        $previous_ext = array_pop($file_parts);
        if ($previous_ext == 'tar') {
          $archive_type = $previous_ext . '.' . $archive_type;
        }
        $xml->release->files->file[$key]->archive_type = $archive_type;
        $xml->release->files->file[$key]->md5 = $file->field_release_file_hash->value(['sanitize' => FALSE]);
        $xml->release->files->file[$key]->size = $file->field_release_file->value()['filesize'];
        $xml->release->files->file[$key]->filedate = $file->field_release_file->value()['timestamp'];
      }

      $tids = [];
      foreach ($vocabularies as $vocabulary) {
        if ($vocabulary->vid == $api_vocabulary) {
          // Do not include API compatibility term.
          continue;
        }
        if (isset($release->{'taxonomy_' . $vocabulary->machine_name}[LANGUAGE_NONE])) {
          foreach ($release->{'taxonomy_' . $vocabulary->machine_name}[LANGUAGE_NONE] as $term) {
            $tids[] = $term['tid'];
          }
        }
      }
      if (!empty($tids)) {
        foreach (array_values(taxonomy_term_load_multiple($tids)) as $key => $term) {
          $xml->release->terms->term[$key]->name = $vocabularies[$term->vocabulary_machine_name]->name;
          $xml->release->terms->term[$key]->value = $term->name;
        }
      }

      $files = ['all'];
      drupal_alter('project_release_xml_release', $xml, $project, $release, $files);

      foreach ($files as $file) {
        $releases_xml[$file][$release_wrapper->field_release_build_type->value()][$release->nid] = $xml;
      }
    }
  }

  foreach ($releases_xml as $file => $release_xml) {
    $xml = new SimpleXMLElement('<root></root>');
    $xml->title = $project->title;
    $xml->short_name = $project->field_project_machine_name[LANGUAGE_NONE][0]['value'];
    $xml->{'dc:creator'} = $project->name;
    $xml->type = $project->type;

    if ($project->status) {
      drupal_alter('project_release_xml', $xml, $project, $file, $release_xml);

      $xml->link = url('node/' . $project->nid, array('absolute' => TRUE));

      // To prevent the update(_status) module from having problems parsing the
      // XML, the terms need to be at the end of the information for the
      // project.
      $tids = array();
      foreach (taxonomy_get_vocabularies() as $vocabulary) {
        if (isset($project->{'taxonomy_' . $vocabulary->machine_name}[LANGUAGE_NONE])) {
          foreach ($project->{'taxonomy_' . $vocabulary->machine_name}[LANGUAGE_NONE] as $term) {
            $tids[] = $term['tid'];
          }
        }
      }
      if (!empty($tids)) {
        $n = 0;
        if (isset($extra_terms[$project->type])) {
          $xml->terms->term[$n]->name = 'Projects';
          $xml->terms->term[$n]->value = $extra_terms[$project->type];
          $n += 1;
        }
        foreach (taxonomy_term_load_multiple($tids) as $term) {
          $xml->terms->term[$n]->name = $vocabularies[$term->vocabulary_machine_name]->name;
          $xml->terms->term[$n]->value = $term->name;
          $n += 1;
        }
      }
    }
    else {
      $xml->project_status = 'unpublished';
    }

    $string = '';
    foreach ($xml->children() as $child) {
      $string .= $child->asXML() . "\n";
    }

    if (!empty($release_xml)) {
      $string .= "<releases>\n";
      if (isset($release_xml['static'])) {
        foreach ($release_xml['static'] as $release_xml_chunk) {
          $string .= $release_xml_chunk->release->asXML();
        }
      }
      // Keep dev releases at the end, ordering of releases is significant to
      // the Drupal core update status client.
      if (isset($release_xml['dynamic'])) {
        foreach ($release_xml['dynamic'] as $release_xml_chunk) {
          $string .= $release_xml_chunk->release->asXML();
        }
      }
      $string .= "</releases>\n";
    }

    project_release_history_write_xml($string, $project, $file);
  }
}

/**
 * Add backward compatibility project type term.
 *
 * Drupal.org-specific hack, see https://drupal.org/node/2126123. Drush
 * expects taxonomy terms which no longer exist on Drupal.org.
 */
function _project_release_compatibility_term($project) {
  $extra_terms = array(
    'project_core' => "   <term><name>Projects</name><value>Drupal core</value></term>\n",
    'project_distribution' => "   <term><name>Projects</name><value>Distributions</value></term>\n",
    'project_module' => "   <term><name>Projects</name><value>Modules</value></term>\n",
    'project_theme' => "   <term><name>Projects</name><value>Themes</value></term>\n",
    'project_theme_engine' => "   <term><name>Projects</name><value>Theme engines</value></term>\n",
    'project_translation' => "   <term><name>Projects</name><value>Translations</value></term>\n",
  );

  return isset($extra_terms[$project->type]) ? $extra_terms[$project->type] : '';
}

/**
 * Write out the XML history for a given project and version to a file.
 *
 * @param string $xml
 *   The XML representation of the history.
 * @param object $project
 *   Containing (at least) the title and field_project_machine_name of the
 *   project, or NULL for all projects list.
 * @param string $file
 *   The history file being written.
 */
function project_release_history_write_xml($xml, stdClass $project = NULL, $file = NULL) {
  // Dublin core namespace according to
  // http://dublincore.org/documents/dcmi-namespace/
  $dc_namespace = 'xmlns:dc="http://purl.org/dc/elements/1.1/"';
  $full_xml = '<?xml version="1.0" encoding="utf-8"?>' . "\n";
  if (is_null($project)) {
    // We are outputting a global project list.
    $project_dir = 'project-list';
    $filename = 'project-list-all.xml';
    $full_xml .= '<projects ' . $dc_namespace . ">\n" . $xml . "</projects>\n";
  }
  else {
    // Setup the filenames we'll be using.  Normally, we'd have to be extra
    // careful with field_project_machine_name to avoid malice here, however,
    // that's validated on the project edit form to prevent any funny
    // characters, so that much is safe.  The rest of these paths are just from
    // the global variables at the top of this script, so we can trust those.
    // The only one we should be careful of is the taxonomy term for the API
    // compatibility.
    $project_dir = $project->field_project_machine_name[LANGUAGE_NONE][0]['value'];
    $filename = $project->field_project_machine_name[LANGUAGE_NONE][0]['value'] . '-' . strtr($file, '/', '_') . '.xml';
    $full_xml .= '<project ' . $dc_namespace . ">\n" . $xml . "</project>\n";
  }

  // Make sure we've got the right project-specific subdirectory.
  $project_dir = file_build_uri(variable_get('project_release_history_root', 'release-history') . '/' . $project_dir);
  if (!file_prepare_directory($project_dir, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
    $wrapper = file_stream_wrapper_get_instance_by_uri($project_dir);
    return drush_set_error('NO_DIRECTORY', dt('@path does not exist or is not writable.', array('@path' => $wrapper->realpath())));
  }

  // Write out file.
  $filepath = $project_dir . '/' . $filename;
  if (file_exists($filepath) && file_get_contents($filepath) === $full_xml) {
    drush_log(dt('No changes for @filename', ['@filename' => $filename]));
    return;
  }
  if (file_unmanaged_save_data($full_xml, $filepath, FILE_EXISTS_REPLACE)) {
    module_invoke_all('project_release_history_xml_written', $project, $file);
  }
  else {
    $wrapper = file_stream_wrapper_get_instance_by_uri($filepath);
    return drush_set_error('FILE_WRITE', dt('Can\'t write to @file.', array('@file' => $wrapper->realpath())));
  }
}
