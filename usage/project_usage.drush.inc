<?php

/**
 * @file
 * Update the project_usage statistics incrementally from logs.
 *
 * @author Brandon Bergren (http://drupal.org/user/53081)
 *
 * Originally based on project-usage-process.php. Rewritten for drush by
 * Brandon Bergren.
 * @author Andrew Morton (http://drupal.org/user/34869)
 * @author Derek Wright (http://drupal.org/user/46549)
 */

/**
 * Implements hook_drush_help().
 */
function project_usage_drush_help($section) {
  switch ($section) {
    case 'meta:project_usage:title':
      return dt('Project Usage Statistics commands');

    case 'meta:project_usage:summary':
      return dt('Commands for processing usage statistics');

    case 'project_usage:import-usage-stats':
      return dt('Import pre-aggregated usage stats.');
  }
}

/**
 * Implements hook_drush_command().
 */
function project_usage_drush_command() {
  $items = array();

  $items['import-usage-stats'] = array(
    'description' => 'Import pre-aggregated usage stats.',
    'drupal dependencies' => array('project_usage'),
    'examples' => array(
      'drush import-usage-stats' => 'Import pre-aggregated usage stats.',
    ),
    'arguments' => array(
      'weektimestamp' => 'Process which weeks worth of data?',
    ),
    'options' => array(
      'count-file-path' => 'The path on the server of the count files.',
    ),
  );

  return $items;
}

/**
 * Open a file, transparently handling compression.
 *
 * @param string $filename
 *   Name of the file to open.
 *
 * @return resource
 *   Returns a file pointer resource on success, or FALSE on failure.
 */
function _project_usage_open_file($filename) {
  if (substr($filename, -3) == '.gz') {
    return fopen('compress.zlib://' . $filename, 'r');
  }
  return fopen($filename, 'r');
}

/**
 * Gets data about projects, releases, and core versions.
 */
function project_usage_retrieve_metadata() {
  static $data = array();

  if (empty($data)) {
    // Load information about projects.
    $result = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', project_project_node_types())
      ->fieldCondition('field_project_has_releases', 'value', TRUE)
      ->execute();
    if (isset($result['node'])) {
      foreach (array_chunk(array_keys($result['node']), 200) as $project_nids) {
        foreach (node_load_multiple($project_nids) as $project) {
          $project_name = $project->field_project_machine_name[LANGUAGE_NONE][0]['value'];
          $projnames[$project->nid] = $project_name;
          $projnids[$project_name] = $project->nid;
        }
        drupal_static_reset();
      }
    }

    // Load information about releases.
    $result = (new EntityFieldQuery())->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', project_release_release_node_types())
      ->execute();
    if (isset($result['node'])) {
      foreach (array_chunk(array_keys($result['node']), 200) as $release_nids) {
        foreach (node_load_multiple($release_nids) as $release) {
          $project_name = $projnames[$release->field_release_project[LANGUAGE_NONE][0]['target_id']];
          $releasedata[$project_name][$release->field_release_version[LANGUAGE_NONE][0]['value']]['release_nid'] = $release->nid;
        }
        drupal_static_reset();
      }
    }

    $data['projects'] = $projnids;
    $data['releases'] = $releasedata;
  }

  return $data;
}

/**
 * Run the counts for projects and releases, and update the database.
 */
function drush_project_usage_import_usage_stats($week) {
  module_load_include('inc', 'project_usage', 'includes/date_api');
  drush_log(dt("Starting @week tallies on database.", array('@week' => $week)));

  $count_file_path = drush_get_option('count-file-path');
  if (!$count_file_path) {
    $count_file_path = variable_get('project_usage_count_file_path', '/var/log/updatestats/counts');
  }

  $metadata = project_usage_retrieve_metadata();

  drush_log(dt('Reading usage counts.'));
  $project_releasecounts_fh = _project_usage_open_file($count_file_path . "/" . $week . '.releasecounts');
  $release_counts = [];
  while ($line = fgets($project_releasecounts_fh)) {
    $nid = 0;
    $term = 0;
    // Split up $line into its components.
    list($count, $pdata) = explode(' ', trim($line), 2);
    list($project_machine_name, $rel_ver) = explode('|', $pdata, 3);

    // Skip projects that are not ours.
    if (empty($metadata['releases'][$project_machine_name][$rel_ver]['release_nid'])) {
      continue;
    }

    // Add up counts.
    $nid = $metadata['releases'][$project_machine_name][$rel_ver]['release_nid'];
    if (isset($release_counts[$project_machine_name][$nid])) {
      $release_counts[$project_machine_name][$nid] += $count;
    }
    else {
      $release_counts[$project_machine_name][$nid] = $count;
    }
  }
  fclose($project_releasecounts_fh);

  // Project release counts.
  drush_log(dt('Importing release stats.'));
  // Clear out existing data.
  db_delete('project_usage_week_release')
    ->condition('timestamp', $week)
    ->execute();
  // Load in new counts.
  $qcount = 0;
  $query = db_insert('project_usage_week_release')
    ->fields(['nid', 'project_nid', 'timestamp', 'count']);
  foreach ($release_counts as $project_machine_name => $releases) {
    foreach ($releases as $nid => $count) {
      $qcount++;
      $query->values([
        'nid' => $nid,
        'project_nid' => $metadata['projects'][$project_machine_name],
        'timestamp' => $week,
        'count' => $count,
      ]);
    }

    if ($qcount > 1000) {
      $query->execute();
      $qcount = 0;
      $query = db_insert('project_usage_week_release')
        ->fields(['nid', 'project_nid', 'timestamp', 'count']);
    }
  }
  if ($qcount) {
    $query->execute();
  }

  drush_log(dt("End updating   @week tallies on database.", array('@week' => $week)));
  project_usage_get_active_weeks(TRUE);
}
